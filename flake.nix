{

  inputs = {
    nixpkgs.follows = "nix/nixpkgs";
  };

  outputs = { self, nixpkgs, nix, ... }:
  let

    supportedSystems = [ "x86_64-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);

  in {

    overlay = final: prev: {

        midimonster = with final; stdenv.mkDerivation {
          name = "midimonster";
          buildInputs = with pkgs; [ pkg-config jack2 gnumake alsaLib python3 udev lua5_3 openssl ncurses libevdev git ];
          makeFlags = [ "PLUGINS=$(out)/lib/midimonster" "DEFAULT_CFG=$(out)/share/midimonster/default.cfg" ];
          installFlags = [ "PREFIX=$(out)" ];
          src = fetchFromGitHub {
            owner = "cbdevnet";
            repo = "midimonster";
            rev = "8a67d5dfcbff5cad28bdb86c5c8a1cc85ebdc288";
            sha256 = "g/4bT1H7fiuLoSmQXDAuvZ0GAAyLpwpKPGDlkXhscIg=";
            };
          };

        agordejo = with final; let nix = final.nix; in qt5.mkDerivation {

          name = "agordejo";
          propagatedBuildInputs = [ (python36.withPackages (ps: with ps; [ pyqt5 Nuitka ])) glibc dejavu_fonts new-session-manager ];
          nativeBuildInputs = [ python36Packages.wrapPython ];
          pythonPath = with python36Packages; [ pyqt5 ];
          dontWrapQtApps = true;
          prePhases = [ "fixHome" ];
          fixHome = "export HOME=$(mktemp -d)";
          configurePhase = ''
            bash ./configure --prefix=$out
            sed 's/--recurse-not-to=PyQt5 /--enable-plugin=qt-plugins /g' -i Makefile
          '';
          preFixup = ''
            wrapProgram "$out/bin/agordejo" --prefix PYTHON_PATH : $pythonPath
          '';
          src = fetchgit {
            url = "https://git.laborejo.org/lss/Agordejo.git";
            sha256 = "d3eb539a556352f3f47881d71fb0e5777b2f3e9a4251d283c18c67ce996774b7";
            rev = "7ac1790f31b005b93f3e7bc032005bdcb8cc1fcb";
          };

        };

    };

    packages = forAllSystems (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in with pkgs; {
        inherit midimonster;
        agordejo = pkgs.agordejo;
    });

  };

}
